package com.gb.counselinglistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CounselingListApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CounselingListApiApplication.class, args);
	}

}
