package com.gb.counselinglistapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AvailableTime {
    AM10("10:00~11:00", true),
    AM11("11:00~12:00", true),
    AM13("13:00~14:00", false),
    AM14("14:00~15:00", false),
    AM15("15:00~16:00", false),
    AM16("16:00~17:00", false);

    private String time;
    private Boolean isAM;
}
