package com.gb.counselinglistapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CounselingListItem {
    private Long id;
    private String name;
    private String genderName;
    private LocalDate birthDate;
}
