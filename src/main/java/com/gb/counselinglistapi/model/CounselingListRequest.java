package com.gb.counselinglistapi.model;

import com.gb.counselinglistapi.enums.AvailableTime;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CounselingListRequest {
    private String name;
    private Boolean gender;
    private LocalDate birthDate;
    private String phoneNumber;
    @Enumerated(value = EnumType.STRING)
    private AvailableTime availableTime;
}
