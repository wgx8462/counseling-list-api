package com.gb.counselinglistapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CounselingListGenderChangeRequest {
    @Column(nullable = false)
    private Boolean gender;
}
