package com.gb.counselinglistapi.service;

import com.gb.counselinglistapi.entity.CounselingList;
import com.gb.counselinglistapi.model.CounselingListGenderChangeRequest;
import com.gb.counselinglistapi.model.CounselingListItem;
import com.gb.counselinglistapi.model.CounselingListRequest;
import com.gb.counselinglistapi.model.CounselingListResponse;
import com.gb.counselinglistapi.repository.CounselingListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CounselingListService {
    private final CounselingListRepository counselingListRepository;

    public void setCounselingList(CounselingListRequest request) {
        CounselingList addData = new CounselingList();
        addData.setName(request.getName());
        addData.setGender(request.getGender());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAvailableTime(request.getAvailableTime());

        counselingListRepository.save(addData);
    }

    public List<CounselingListItem> getCounselingLists() {
        List<CounselingList> originList = counselingListRepository.findAll();

        List<CounselingListItem> result = new LinkedList<>();

        for (CounselingList counselingList : originList) {
            CounselingListItem addItem = new CounselingListItem();
            addItem.setId(counselingList.getId());
            addItem.setName(counselingList.getName());
            addItem.setGenderName(counselingList.getGender() ? "남자" : "여자");
            addItem.setBirthDate(counselingList.getBirthDate());

            result.add(addItem);
        }

        return result;

    }

    public CounselingListResponse getCounselingList(long id) {
        CounselingList originData = counselingListRepository.findById(id).orElseThrow();

        CounselingListResponse response = new CounselingListResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setGenderName(originData.getGender() ? "남자" : "여자");
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAvailableTimeName(originData.getAvailableTime().getTime());
        response.setAvailableIsAmName(originData.getAvailableTime().getIsAM() ? "오전" : "오후");

        return response;
    }

    public void putCounselingListGender(long id, CounselingListGenderChangeRequest request) {
        CounselingList addData = counselingListRepository.findById(id).orElseThrow();
        addData.setGender(request.getGender());

        counselingListRepository.save(addData);

    }
}
