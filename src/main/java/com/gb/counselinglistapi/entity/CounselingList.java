package com.gb.counselinglistapi.entity;

import com.gb.counselinglistapi.enums.AvailableTime;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class CounselingList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false)
    private Boolean gender;
    @Column(nullable = false)
    private LocalDate birthDate;
    @Column(nullable = false, length = 15)
    private String phoneNumber;
    @Column(nullable = false, length = 6)
    @Enumerated(value = EnumType.STRING)
    private AvailableTime availableTime;
}
