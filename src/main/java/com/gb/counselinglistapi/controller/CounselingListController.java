package com.gb.counselinglistapi.controller;

import com.gb.counselinglistapi.model.CounselingListGenderChangeRequest;
import com.gb.counselinglistapi.model.CounselingListItem;
import com.gb.counselinglistapi.model.CounselingListRequest;
import com.gb.counselinglistapi.model.CounselingListResponse;
import com.gb.counselinglistapi.service.CounselingListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/counseling-list")
public class CounselingListController {
    private final CounselingListService counselingListService;

    @PostMapping("/new")
    public String setCounselingList(@RequestBody CounselingListRequest request) {
        counselingListService.setCounselingList(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<CounselingListItem> getCounselingLists() {
        return counselingListService.getCounselingLists();
    }

    @GetMapping("/detail/{id}")
    public CounselingListResponse getCounselingList(@PathVariable long id) {
        return counselingListService.getCounselingList(id);
    }

    @PutMapping("/gender/{id}")
    public String putCounselingListGender(long id, CounselingListGenderChangeRequest request) {
        counselingListService.putCounselingListGender(id, request);

        return "Ok";
    }
}
