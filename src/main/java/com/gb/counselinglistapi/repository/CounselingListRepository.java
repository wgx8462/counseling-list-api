package com.gb.counselinglistapi.repository;

import com.gb.counselinglistapi.entity.CounselingList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CounselingListRepository extends JpaRepository<CounselingList, Long> {
}
